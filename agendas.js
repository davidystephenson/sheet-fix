const { convertArrayToCSV } = require('convert-array-to-csv')
const fs = require('fs')

const agendas = require('./agendas.json')

function add (entry) {
  const [name, details] = entry

  console.log('details test:', details)

  const row = [name, ...details]

  console.log('row test:', row)

  return row
}

const rows = Object
  .entries(agendas)
  .map(add)

console.log('rows test:', rows)

const csv = convertArrayToCSV(rows)

fs.writeFileSync('./agendas.csv', csv)

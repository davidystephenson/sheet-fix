const converter = require('json-2-csv')
const fs = require('fs')

const brains = require('./brains')

const dracula = require('./dracula')

console.log('brains.length test:', brains.length)
console.log('dracula.length test:', dracula.length)

const output = {
  merges: 0,
  matches: 0
}

function set (output, data) {
  for (const key in data) {
    const old = output[key]

    if (!old) {
      const now = data[key]

      output[key] = now
    }
  }
}

function combine (a, b) {
  const output = {}

  set(output, a)
  set(output, b)

  return output
}

function merge (rows) {
  rows.forEach(row => {
    const {
      'Address 1': Address1,
      'Address 2*': Address2,
      Name,
      Email
    } = row
    const key = `${Address1}_${Name}_${Email}_${Address2}`

    const entry = output[key]
    if (entry) {
      const merged = combine(entry, row)

      // console.log('merge:', Name)

      console.log('POWER BONUS:', Name)

      output[key] = merged
      output.merges = output.merges + 1
    } else {
      const keys = Object.keys(output)
      const match = keys.find(key =>
        key.includes(Address1) && key.includes(Address2)
      )

      if (match) {
        output.matches = output.matches + 1
      }

      output[key] = row
    }
  })
}

merge(brains)
merge(dracula)

const rows = []
for (const key in output) {
  const row = output[key]

  rows.push(row)
}

console.log('Rows:', rows.length)

converter
  .json2csvAsync(rows, { emptyFieldValue: '' })
  .then(csv => {
    fs.writeFileSync('./output.csv', csv)
  })
  .catch(error => {
    throw error
  })

console.log('Merges:', output.merges)
console.log('Matches:', output.matches)
